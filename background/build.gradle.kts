plugins {
    kotlin("js")
    kotlin("plugin.serialization")
}

dependencies {
    implementation(kotlin("stdlib-js"))
    implementation(project(":chrome"))
    implementation(project(":common"))
    implementation(project(":translation"))
    implementation(Deps.Kotlin.serialization)
    compileOnly(npm("extpay", "3.0.7"))
}

kotlin {
    js(IR) {
        binaries.executable()
        browser {
            distribution {
                directory = File("$rootDir/build/distributions/")
            }
        }
    }
}
