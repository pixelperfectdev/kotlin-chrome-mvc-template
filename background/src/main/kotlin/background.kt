import chrome.sendMessageToTabs
import com.pixelperfect.example.common.koinInit
import com.pixelperfect.example.translation.Translation
import com.pixelperfect.example.common.getKoin
import com.pixelperfect.example.data.Action
import com.pixelperfect.example.data.Command
import di.backgroundModule
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

fun main() {
    koinInit(backgroundModule)

    chrome.runtime.onMessage.addListener { request, sender, sendResponse ->
        println("Background receives a message: ${request}")
        val command = Json.decodeFromString<Command>(request.toString())
        when (command.action.toString()) {
            Action.CLOSE_CONTENT_VIEW.toString() -> sendMessageToTabs(Command(action = Action.CLOSE_CONTENT_VIEW))
            Action.OPEN_OPTIONS.toString() -> chrome.runtime.openOptionsPage()
            Action.SHOW_CONTENT_VIEW.toString() -> sendMessageToTabs(Command(action = Action.SHOW_CONTENT_VIEW))
            else -> sendMessageToTabs(Command(action = Action.UNKNOWN))
        }
    }

//    chrome.commands.onCommand.addListener { command ->
//        println("Background receives a command: ${command}")
//        val action = Action.parse(command)
//        when (action) {
//            Action.SHOW_CONTENT_VIEW -> sendMessageToTabs(Command(action = Action.SHOW_CONTENT_VIEW))
//            else -> {}
//        }
//    }


//    chrome.contextMenus.create(createProperties = PageClickCreateProperties(translation))
//    chrome.contextMenus.onClicked.addListener { info: dynamic, tab: dynamic ->
//        when (info.menuItemId) {
//            else -> {}
//        }
//    }

    chrome.action.onClicked.addListener {
        sendMessageToTabs(Command(action = Action.SHOW_CONTENT_VIEW))
    }

    chrome.runtime.onInstalled.addListener { details ->
        when (details.reason.uppercase()) {
            OnInstalledReason.INSTALL.name.uppercase(),
            OnInstalledReason.UPDATE.name.uppercase() ->
                chrome.runtime.setUninstallURL("https://tally.so/r/mKzRZ7")
        }
    }

    js("importScripts('ExtPay.js')")
    val extpay = ExtPay("sum-up-now")
    extpay.startBackground()
}
external class ExtPay(id: dynamic) {
    fun startBackground()
}

// Context Menus

//private fun PageClickCreateProperties(translation: Translation): dynamic {
//    val properties = js("{}")
//    properties.id = MenuAction.PageClick.name
//    properties.title = translation.menu.pageClick
//    properties.contexts = arrayOf("page")
//    return properties
//}

private enum class OnInstalledReason {
    INSTALL,
    UPDATE,
}