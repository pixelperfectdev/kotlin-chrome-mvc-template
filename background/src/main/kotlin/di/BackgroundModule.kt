package di

import com.pixelperfect.example.translation.Translation
import org.koin.dsl.module

val backgroundModule = module {

    single<Translation> { Translation() }

}