package chrome

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToDynamic

inline fun <reified T> sendMessageToTabs(message: T, noinline callback: ((Any) -> Unit)? = null) {
    val queryInfo = ActiveQueryTab()
    chrome.tabs.query(queryInfo = Json.encodeToDynamic(queryInfo)) { tabs ->
        tabs.forEach {  tab ->
            if (tab.id != null && tab.active && tab.selected) {
                chrome.tabs.sendMessage(
                    tabId = tab.id!!,
                    message = Json.encodeToString(message),
                )
            }
        }
    }
}

inline fun <reified T> sendMessageToBackground(message: T, noinline callback: ((Any) -> Unit) = {}) {
    chrome.runtime.sendMessage(
        message = Json.encodeToString(message),
        responseCallback = callback
    )
}

@Serializable
data class ActiveQueryTab(
    @SerialName("active") var active: Boolean? = true,
    @SerialName("currentWindow") var currentWindow: Boolean? = true,
)