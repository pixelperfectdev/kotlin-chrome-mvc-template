package chrome

external class Intl {
    class DisplayNames(list: dynamic, obj: dynamic) {
        fun of(value: String): String
    }
}