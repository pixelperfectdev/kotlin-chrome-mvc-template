package chrome.audio

import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Float32Array
import org.w3c.dom.HTMLMediaElement
import org.w3c.dom.events.Event
import org.w3c.dom.mediacapture.MediaStream

external class AudioContext {
    constructor(contextOptions: AudioContextOptions? = definedExternally)
    fun createBuffer(numberOfChannels: Int, length: Int, sampleRate: Number): AudioBuffer
    fun createBufferSource(): AudioBufferSourceNode
    fun decodeAudioData(audioData: ArrayBuffer, successCallback: (decodedData: AudioBuffer) -> Unit, errorCallback: ((error: Any?) -> Unit)? = definedExternally)
//    fun createGain(): GainNode
//    fun createDelay(maxDelayTime: Double? = definedExternally): DelayNode
//    fun createBiquadFilter(): BiquadFilterNode
//    fun createAnalyser(): AnalyserNode
//    fun createWaveShaper(): WaveShaperNode
//    fun createPanner(): PannerNode
//    fun createConvolver(): ConvolverNode
//    fun createDynamicsCompressor(): DynamicsCompressorNode
//    fun createOscillator(): OscillatorNode
//    fun createPeriodicWave(real: Float32Array, imag: Float32Array, constraints: PeriodicWaveConstraints? = definedExternally): PeriodicWave
//    fun createScriptProcessor(bufferSize: Int, numberOfInputChannels: Int? = definedExternally, numberOfOutputChannels: Int? = definedExternally): ScriptProcessorNode
//    fun createStereoPanner(): StereoPannerNode
//    fun createChannelSplitter(numberOfOutputs: Int? = definedExternally): ChannelSplitterNode
//    fun createChannelMerger(numberOfInputs: Int? = definedExternally): ChannelMergerNode
//    fun createMediaElementSource(mediaElement: HTMLMediaElement): MediaElementAudioSourceNode
//    fun createMediaStreamSource(mediaStream: MediaStream): MediaStreamAudioSourceNode
//    fun createMediaStreamDestination(): MediaStreamAudioDestinationNode
//    fun createGainNode(): GainNode
//    fun createDynamicsCompressorNode(): DynamicsCompressorNode
//    fun createDelayNode(maxDelayTime: Double? = definedExternally): DelayNode
//    fun createConstantSource(): ConstantSourceNode
    fun createBufferSource(buffer: AudioBuffer? = definedExternally, config: AudioBufferSourceOptions? = definedExternally): AudioBufferSourceNode
//    fun createIIRFilter(feedforward: Float32Array, feedback: Float32Array): IIRFilterNode
//    fun createIIRFilter(feedforward: Array<Float32Array>, feedback: Array<Float32Array>): IIRFilterNode
    fun resume()
    fun close()
    fun suspend()
    val currentTime: Double
    val destination: AudioDestinationNode
    val sampleRate: Number
    val listener: AudioListener
    val state: String
}

external interface AudioDestinationNode : AudioNode {
    val maxChannelCount: Int
//    val channelCountMode: ChannelCountMode
//    val channelInterpretation: ChannelInterpretation
}

external interface AudioNode {
    val context: AudioContext
    val numberOfInputs: Int
    val numberOfOutputs: Int
    fun connect(destination: AudioNode, output: Int = definedExternally, input: Int = definedExternally): AudioNode
//    fun connect(destination: AudioParam, output: Int = definedExternally): Unit
    fun disconnect(output: Int = definedExternally): Unit
    fun disconnect(destination: AudioNode, output: Int = definedExternally, input: Int = definedExternally): Unit
//    fun disconnect(destination: AudioParam, output: Int = definedExternally): Unit
}

external interface AudioListener {
    var positionX: Double
    var positionY: Double
    var positionZ: Double
    var forwardX: Double
    var forwardY: Double
    var forwardZ: Double
    var upX: Double
    var upY: Double
    var upZ: Double
    var context: AudioContext
    var filter: AudioNode
    fun setOrientation(forwardX: Double, forwardY: Double, forwardZ: Double, upX: Double, upY: Double, upZ: Double)
    fun setPosition(x: Double, y: Double, z: Double)
}

external interface AudioContextOptions {
    var latencyHint: String? get() = definedExternally; set(value) = definedExternally
    var sampleRate: Int? get() = definedExternally; set(value) = definedExternally
    var channelCount: Int? get() = definedExternally; set(value) = definedExternally
    var blockDuration: Double? get() = definedExternally; set(value) = definedExternally
    var blockSize: Int? get() = definedExternally; set(value) = definedExternally
}

external interface AudioBufferOptions {
    var length: Int
    var numberOfChannels: Int
    var sampleRate: Number
}

external class AudioBuffer(options: AudioBufferOptions) {
    val length: Int
    val duration: Double
    val sampleRate: Number
    val numberOfChannels: Int
    fun getChannelData(channel: Int): Float32Array
    fun copyFromChannel(destination: Float32Array, channelNumber: Int, startInChannel: Int? = definedExternally)
    fun copyToChannel(source: Float32Array, channelNumber: Int, startInChannel: Int? = definedExternally)
}

external interface AudioBufferSourceOptions {
    var buffer: AudioBuffer? get() = definedExternally; set(value) = definedExternally
    var detune: Number? get() = definedExternally; set(value) = definedExternally
    var playbackRate: Number? get() = definedExternally; set(value) = definedExternally
    var loop: Boolean? get() = definedExternally; set(value) = definedExternally
    var loopStart: Number? get() = definedExternally; set(value) = definedExternally
    var loopEnd: Number? get() = definedExternally; set(value) = definedExternally
    var reverse: Boolean? get() = definedExternally; set(value) = definedExternally
}

external class AudioBufferSourceNode(audioContext: AudioContext, options: AudioBufferSourceOptions = definedExternally) {
    var buffer: AudioBuffer?
//    val detune: AudioParam
//    val playbackRate: AudioParam
    var loop: Boolean
    var loopStart: Double
    var loopEnd: Double
    var playbackState: String
    fun start(when_: Double = definedExternally, offset: Double = definedExternally, duration: Double = definedExternally)
    fun stop(when_: Double = definedExternally)
    fun addEventListener(type: String, listener: (Event) -> Unit, options: dynamic = definedExternally)
    fun removeEventListener(type: String, listener: (Event) -> Unit, options: dynamic = definedExternally)
    fun connect(source: dynamic)
}

external interface AudioListenerOptions {
    var positionX: Double
    var positionY: Double
    var positionZ: Double
    var forwardX: Double
    var forwardY: Double
    var forwardZ: Double
    var upX: Double
    var upY: Double
    var upZ: Double
    var speedOfSound: Double
}

//external class GainNode(context: AudioContext, options: GainOptions = definedExternally) : AudioNode {
//    val gain: AudioParam
//}