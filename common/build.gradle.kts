plugins {
    kotlin("js")
}

repositories {
    google()
    mavenLocal()
    mavenCentral()
    gradlePluginPortal()
}

dependencies {
    api(Deps.Di.Koin.core)
    api(Deps.Kotlin.Coroutines.core)
    api(Deps.Kotlin.Coroutines.core_js)
    api(Deps.Kotlin.serialization)
    api(project(":data"))
}

kotlin {
    js(IR) {
        binaries.executable()
        browser()
    }
}
