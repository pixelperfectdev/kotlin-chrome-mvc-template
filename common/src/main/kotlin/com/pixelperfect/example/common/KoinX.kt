package com.pixelperfect.example.common

import org.koin.core.Koin
import org.koin.core.context.GlobalContext
import org.koin.core.module.Module

fun koinInit(module: Module) {
    if (GlobalContext.getOrNull() != null) return
    GlobalContext.startKoin {
        allowOverride(true)
        modules(module)
    }
}

fun getKoin(): Koin? {
    return GlobalContext.getOrNull()
}