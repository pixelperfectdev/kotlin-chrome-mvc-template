package com.pixelperfect.example.common

import kotlin.js.Date

fun now(): Date {
    return Date(Date.now())
}
