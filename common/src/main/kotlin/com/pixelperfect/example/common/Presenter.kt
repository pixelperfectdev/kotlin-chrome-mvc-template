package com.pixelperfect.example.common

interface Presenter {
    fun clear()
}