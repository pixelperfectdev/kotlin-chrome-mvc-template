package com.pixelperfect.example.common

inline fun <R, T> Result<T>.flatMap(transform: (T) -> Result<R>): Result<R> {
    return fold(
        onSuccess = { transform(it) },
        onFailure = { Result.failure(it) }
    )
}

inline fun <T> Result<T>.flatRecover(transform: (Throwable) -> Result<T>): Result<T> {
    return fold(
        onSuccess = { Result.success(it) },
        onFailure = { transform(it) }
    )
}