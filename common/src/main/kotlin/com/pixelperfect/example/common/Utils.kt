package com.pixelperfect.example.common

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

inline fun <R> runCatchingCancellable(block: () -> R): Result<R> {
    return try {
        Result.success(block())
    } catch (e: Throwable) {
        if (e is CancellationException) {
            throw e
        }
        Result.failure(e)
    }
}

fun <T> debounce(
    delay: Long = 300L,
    scope: CoroutineScope,
    handler: CoroutineExceptionHandler,
    destinationFunction: (T) -> Unit
): (T) -> Unit {
    var debounceJob: Job? = null
    return { param: T ->
        debounceJob?.cancel()
        debounceJob = scope.launch(handler) {
            delay(delay)
            destinationFunction(param)
        }
    }
}

fun NOT_IMPLEMENTED(): Nothing = throw NotImplementedError()
