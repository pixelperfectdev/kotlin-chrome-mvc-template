package com.pixelperfect.example.common

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.qualifier.named

abstract class ViewModel : KoinComponent {
    val viewModelScope by inject<CoroutineScope>(named(KoinName.COROUTINE_IO))

    open fun onCleared() {
        viewModelScope.cancel()
    }
}