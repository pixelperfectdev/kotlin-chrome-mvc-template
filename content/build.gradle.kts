plugins {
    kotlin("js")
    id("org.jetbrains.compose")
}

dependencies {
    implementation(kotlin("stdlib-js"))
    implementation(compose.web.core)
    implementation(compose.runtime)
    implementation(Deps.Kotlin.serialization)

    implementation(project(":chrome"))
    implementation(project(":common"))
    implementation(project(":domain"))
    implementation(project(":repository"))
    implementation(project(":service"))
    implementation(project(":interactor"))
    implementation(project(":design-system"))
    implementation(project(":translation"))
}

kotlin {
    js(IR) {
        binaries.executable()
        browser {
            distribution {
                directory = File("$rootDir/build/distributions/")
            }
        }
    }
}
