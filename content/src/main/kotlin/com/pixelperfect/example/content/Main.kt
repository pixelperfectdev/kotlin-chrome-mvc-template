package com.pixelperfect.example.content

import com.pixelperfect.example.common.Constants
import com.pixelperfect.example.common.getKoin
import com.pixelperfect.example.common.koinInit
import com.pixelperfect.example.content.di.webAppModule
import com.pixelperfect.example.data.Action
import com.pixelperfect.example.data.Command
import kotlinx.browser.document
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

fun main() {
    // Dependency injection
    koinInit(webAppModule)

    val webApp = getKoin()?.get<WebApp>() ?: return

    chrome.runtime.onMessage.addListener { request, sender, sendResponse ->
        println("Content script receives a massage: $request")
        val command = Json.decodeFromString<Command>(request.toString())
        when (command.action) {
            Action.SHOW_CONTENT_VIEW -> {
                if (!isDialogAdded()) webApp.start()
                else webApp.stop()
            }
            Action.CLOSE_CONTENT_VIEW -> {
                webApp.stop()
            }
            else -> {}
        }
    }
}

private fun isDialogAdded(): Boolean = document.getElementById(Constants.CONTENT_VIEW_ID) != null
