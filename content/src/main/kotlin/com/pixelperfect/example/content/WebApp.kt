package com.pixelperfect.example.content

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import com.pixelperfect.example.common.Constants.CONTENT_VIEW_ID
import com.pixelperfect.example.common.Constants.CONTAINER_ID
import com.pixelperfect.example.common.Constants.DIALOG_ID
import com.pixelperfect.example.content.content.ContentViewModel
import com.pixelperfect.example.design.component.Notification
import com.pixelperfect.example.design.foundation.AppTheme
import com.pixelperfect.example.design.viewstate.NotificationViewState
import kotlinx.browser.document
import kotlinx.dom.addClass
import org.jetbrains.compose.web.renderComposable
import org.koin.core.component.KoinComponent
import org.w3c.dom.OPEN
import org.w3c.dom.ShadowRootInit
import org.w3c.dom.ShadowRootMode

class WebApp(
    private val contentViewModel: ContentViewModel,
): KoinComponent {

    fun start() {
        println("PLOP ${document.URL}")
        val contentView = document.createElement("div")
        contentView.id = CONTENT_VIEW_ID

        val container = document.createElement("div")
        container.id = CONTAINER_ID
        container.addClass(CONTAINER_ID)

        val dialog = document.createElement("div")
        dialog.id = DIALOG_ID
        dialog.addClass(DIALOG_ID)
        container.appendChild(dialog)

        val shadow = contentView.attachShadow(ShadowRootInit(ShadowRootMode.OPEN))
        contentView.shadowRoot?.appendChild(container)

        document.body?.appendChild(contentView)

        renderComposable(dialog) {
            displayApp()
        }
    }

    fun stop() {
        contentViewModel.onStop()
    }

    @Composable
    private fun displayApp() {
        AppTheme {
            val contentViewState = contentViewModel.viewState.collectAsState().value
            val errorState = contentViewModel.errorState.collectAsState().value

            if (errorState.message != null) {
                val banner = remember(errorState.message) {
                        NotificationViewState.Banner.Error(
                        message = errorState.message!!
                    )
                }
                Notification(
                    banner = banner,
                    onDismissed = contentViewModel::onNotificationDismissed
                )
            }
        }
    }
}