package com.pixelperfect.example.content.content

import com.pixelperfect.example.common.ViewModel
import com.pixelperfect.example.domain.presenter.ContentPresenter
import com.pixelperfect.example.domain.usecase.session.SessionUseCase
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class ContentViewModel(
    private val presenter: ContentPresenter,
    private val sessionUseCase: SessionUseCase,
): ViewModel() {

    val viewState = presenter.viewState
    val errorState = presenter.errorState

    private var job: Job? = null

    init {
        viewModelScope.launch { }
    }

    fun onNotificationDismissed() {
        viewModelScope.launch {
            presenter.notificationDismissed()
        }
    }

    fun onStop() {
        job?.cancel()
        viewModelScope.launch {
            sessionUseCase.stop()
        }
    }

    fun onCloseClick() {
        viewModelScope.launch {
            onStop()
        }
    }
}