package com.pixelperfect.example.content.content.presenter

import com.pixelperfect.example.domain.presenter.ContentPresenter
import com.pixelperfect.example.domain.presenter.ContentViewState
import com.pixelperfect.example.domain.presenter.ErrorContentViewState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class ContentPresenterImpl(): ContentPresenter {

    private val _viewState: MutableStateFlow<ContentViewState> = MutableStateFlow(ContentViewState())
    override val viewState = _viewState.asStateFlow()

    private val _errorState: MutableStateFlow<ErrorContentViewState> = MutableStateFlow(ErrorContentViewState())
    override val errorState = _errorState.asStateFlow()

    // Error

    override fun displayError(message: String?) {
        _errorState.value = ErrorContentViewState(message ?: "Unknown error")
    }

    override fun notificationDismissed() {
        _errorState.value = ErrorContentViewState(null)
    }

    // Presenter

    override fun clear() {
        _viewState.value = ContentViewState()
        _errorState.value = ErrorContentViewState(null)
    }
}