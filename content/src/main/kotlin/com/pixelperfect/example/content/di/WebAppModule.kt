package com.pixelperfect.example.content.di

import com.pixelperfect.example.common.Presenter
import com.pixelperfect.example.common.ViewModel
import com.pixelperfect.example.common.commonModule
import com.pixelperfect.example.content.WebApp
import com.pixelperfect.example.content.content.ContentViewModel
import com.pixelperfect.example.content.content.presenter.ContentPresenterImpl
import com.pixelperfect.example.domain.presenter.ContentPresenter
import com.pixelperfect.example.repository.di.repositoryModule
import com.pixelperfect.example.service.di.serviceModule
import com.pixelperfect.example.translation.Translation
import com.pixelperfect.example.interactor.di.interactorModule
import org.koin.dsl.bind
import org.koin.dsl.module

val webAppModule = module {

    single {
        WebApp(
            contentViewModel = get(),
        )
    }

    // Content

    single {
        ContentViewModel(
            presenter = get(),
            sessionUseCase = get(),
        )
    } bind ViewModel::class

    single<ContentPresenter> {
        ContentPresenterImpl()
    } bind Presenter::class

    single<Translation> { Translation() }

    includes(commonModule, repositoryModule, serviceModule, interactorModule)
}