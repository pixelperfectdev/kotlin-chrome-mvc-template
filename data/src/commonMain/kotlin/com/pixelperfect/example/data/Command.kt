package com.pixelperfect.example.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Command(
    @SerialName("action") val action: Action,
    @SerialName("extra") val extra: String? = "",
)

@Serializable
enum class Action {
    @SerialName("show_content_view") SHOW_CONTENT_VIEW,
    @SerialName("close_content_view") CLOSE_CONTENT_VIEW,
    @SerialName("open_options") OPEN_OPTIONS,
    @SerialName("unknown") UNKNOWN;

    companion object{
        fun parse(command: String): Action {
            return try { Action.valueOf(command.uppercase()) }
                catch (e: Throwable) { UNKNOWN }
        }
    }
}

enum class MenuAction {
    SelectText,
    PageClick
}