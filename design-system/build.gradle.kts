plugins {
    kotlin("multiplatform")
    id("org.jetbrains.compose")
}

kotlin {
    js(IR) {
        binaries.executable()
        browser {}
    }

    sourceSets {
        named("commonMain") {
            dependencies {
                implementation(project(":chrome"))
                implementation(project(":translation"))
            }
        }

        named("jsMain") {
            dependencies {
                implementation(compose.web.core)
                implementation(compose.runtime)
            }
        }
    }
}