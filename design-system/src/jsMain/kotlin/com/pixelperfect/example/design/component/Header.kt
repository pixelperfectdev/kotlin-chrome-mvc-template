package com.pixelperfect.example.design.component

import androidx.compose.runtime.Composable
import com.pixelperfect.example.design.foundation.AppTheme
import com.pixelperfect.example.design.foundation.Asset
import com.pixelperfect.example.design.foundation.Containers
import org.jetbrains.compose.web.css.AlignItems
import org.jetbrains.compose.web.css.JustifyContent
import org.jetbrains.compose.web.css.alignItems
import org.jetbrains.compose.web.css.flexGrow
import org.jetbrains.compose.web.css.gap
import org.jetbrains.compose.web.css.justifyContent
import org.jetbrains.compose.web.css.padding
import org.jetbrains.compose.web.dom.Div

@Composable
fun Header(
    contentLeft: @Composable () -> Unit = {},
    contentRight: @Composable () -> Unit = {},
    onCloseIcon: Asset = Asset.IcClose,
    onCloseClick: () -> Unit,
) {
    val size = AppTheme.size
    Div({
        classes(Containers.row, Containers.maxWidth)
        style {
            justifyContent(JustifyContent.SpaceBetween)
            padding(size.S, size.M)
            alignItems(AlignItems.FlexStart)
        }
    }) {
        Div({
            classes(Containers.row)
            style {
                gap(size.SS)
            }
        }) {
            contentLeft()
        }

        Div({
            style {
                flexGrow(1)
            }
        })

        Div({
            classes(Containers.row)
            style {
                gap(size.SS)
            }
        }) {
            contentRight()
            Icon(
                asset = onCloseIcon,
                onIconClick = onCloseClick,
                size = AppTheme.size.L,
                alt = AppTheme.translation.common.close,
            )
        }
    }
}