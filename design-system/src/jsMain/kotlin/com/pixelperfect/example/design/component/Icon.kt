package com.pixelperfect.example.design.component

import androidx.compose.runtime.Composable
import chrome.runtime.getURL
import com.pixelperfect.example.design.foundation.AppTheme
import com.pixelperfect.example.design.foundation.Asset
import org.jetbrains.compose.web.css.CSSNumeric
import org.jetbrains.compose.web.css.height
import org.jetbrains.compose.web.css.margin
import org.jetbrains.compose.web.css.padding
import org.jetbrains.compose.web.css.px
import org.jetbrains.compose.web.css.width
import org.jetbrains.compose.web.dom.Img

@Composable
fun Icon(
    asset: Asset,
    size: CSSNumeric,
    alt: String,
    onIconClick: (() -> Unit)? = null,
    tint: Boolean = true,
    customTint: String? = null,
    styles: Array<String> = arrayOf()
) {
    val style = AppTheme.style
    val colors = AppTheme.colors
    Tooltip(
        title = alt
    ) {
        Img(
            src = getURL("icons/${asset.name.toSnakeCase()}.svg"),
            alt = alt,
            attrs = {
                val defaultStyles = mutableListOf<String>()
                if (onIconClick != null) defaultStyles.add(style.clickable)
                if (customTint != null) defaultStyles.add(customTint)
                else if (tint && colors.isDarkMode) defaultStyles.add(style.tintDarkTheme)
                else if (tint && !colors.isDarkMode) defaultStyles.add(style.tintLightTheme)

                classes(*defaultStyles.toTypedArray(), *styles)
                style {
                    width(size)
                    height(size)
                    padding(0.px)
                    margin(0.px)
                }
                if (onIconClick != null) onClick {
                    it.stopPropagation()
                    onIconClick()
                }
            }
        )
    }
}

private fun String.toSnakeCase() = fold(StringBuilder(length)) { acc, c ->
    if (c in 'A'..'Z') (if (acc.isNotEmpty()) acc.append('_') else acc).append(c + ('a' - 'A'))
    else acc.append(c)
}.toString()
