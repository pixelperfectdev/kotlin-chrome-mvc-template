package com.pixelperfect.example.design.component

import androidx.compose.runtime.Composable
import com.pixelperfect.example.design.foundation.AppTheme
import com.pixelperfect.example.design.foundation.Containers
import org.jetbrains.compose.web.css.AlignItems
import org.jetbrains.compose.web.css.JustifyContent
import org.jetbrains.compose.web.css.alignItems
import org.jetbrains.compose.web.css.justifyContent
import org.jetbrains.compose.web.dom.Div
import org.jetbrains.compose.web.dom.Section

@Composable
fun ContainerInSection(content: @Composable () -> Unit) {
    val style = AppTheme.style
    Section({
        classes(Containers.maxWidth, Containers.column)
        style {
            alignItems(AlignItems.Center)
            justifyContent(JustifyContent.Center)
        }
    }) {
        Div({
            classes(style.container.containerInSection)
        }) {
            content()
        }
    }
}