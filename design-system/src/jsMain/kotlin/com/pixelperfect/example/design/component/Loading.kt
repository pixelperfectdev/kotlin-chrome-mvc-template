package com.pixelperfect.example.design.component

import androidx.compose.runtime.Composable
import com.pixelperfect.example.design.foundation.AppTheme
import com.pixelperfect.example.design.foundation.Containers
import org.jetbrains.compose.web.ExperimentalComposeWebApi
import org.jetbrains.compose.web.css.AlignItems
import org.jetbrains.compose.web.css.AnimationTimingFunction
import org.jetbrains.compose.web.css.CSSColorValue
import org.jetbrains.compose.web.css.CSSNumeric
import org.jetbrains.compose.web.css.DisplayStyle
import org.jetbrains.compose.web.css.FlexDirection
import org.jetbrains.compose.web.css.JustifyContent
import org.jetbrains.compose.web.css.LineStyle
import org.jetbrains.compose.web.css.StyleSheet
import org.jetbrains.compose.web.css.alignItems
import org.jetbrains.compose.web.css.animation
import org.jetbrains.compose.web.css.border
import org.jetbrains.compose.web.css.borderRadius
import org.jetbrains.compose.web.css.color
import org.jetbrains.compose.web.css.deg
import org.jetbrains.compose.web.css.display
import org.jetbrains.compose.web.css.duration
import org.jetbrains.compose.web.css.flexDirection
import org.jetbrains.compose.web.css.gap
import org.jetbrains.compose.web.css.height
import org.jetbrains.compose.web.css.iterationCount
import org.jetbrains.compose.web.css.justifyContent
import org.jetbrains.compose.web.css.margin
import org.jetbrains.compose.web.css.percent
import org.jetbrains.compose.web.css.s
import org.jetbrains.compose.web.css.style
import org.jetbrains.compose.web.css.timingFunction
import org.jetbrains.compose.web.css.transform
import org.jetbrains.compose.web.css.width
import org.jetbrains.compose.web.dom.Div

@Composable
fun LoadingView(
    onCancelClick: (() -> Unit)? = null
) {
    val size = AppTheme.size
    Div({
        style {
            display(DisplayStyle.Flex)
            flexDirection(FlexDirection.Column)
            alignItems(AlignItems.Center)
            margin(size.XXXXL)
            justifyContent(JustifyContent.Center)
            gap(size.M)
        }
    }) {
        LoadingSpinner()

        if (onCancelClick != null) TextButton(
            text = AppTheme.translation.common.cancel,
            onButtonClick = onCancelClick
        )
    }
}

@Composable
fun LoadingSpinner(
    loaderColor: CSSColorValue = AppTheme.colors.loader,
    size: CSSNumeric = AppTheme.size.XXL,
    strokeSize: CSSNumeric = AppTheme.size.XS,
) {
    Div({
        classes(LoadingSpinnerStylesheet.spinner)
        style {
            border {
                width(strokeSize)
                color(loaderColor)
                style(LineStyle.Solid)
                borderRadius(size)
            }
            width(size)
            height(size)
            property("border-right-color", "transparent")
            property("border-bottom-color", "transparent")
        }
    })
}

object LoadingSpinnerStylesheet : StyleSheet(Containers) {
    @OptIn(ExperimentalComposeWebApi::class)
    private val rotationKeyframes by keyframes {
        each(0.percent) { transform { rotate(0.deg) } }
        each(100.percent) { transform { rotate(360.deg) } }
    }
    val spinner by style {
        animation(rotationKeyframes) {
            duration(1.s)
            timingFunction(AnimationTimingFunction.Linear)
            iterationCount(null)
        }
    }
}