package com.pixelperfect.example.design.component

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import com.pixelperfect.example.design.foundation.AppTheme
import com.pixelperfect.example.design.foundation.Containers
import com.pixelperfect.example.design.viewstate.BannerState
import com.pixelperfect.example.design.viewstate.NotificationViewState
import kotlinx.browser.window
import org.jetbrains.compose.web.ExperimentalComposeWebApi
import org.jetbrains.compose.web.css.AnimationFillMode
import org.jetbrains.compose.web.css.AnimationTimingFunction
import org.jetbrains.compose.web.css.Position
import org.jetbrains.compose.web.css.StyleSheet
import org.jetbrains.compose.web.css.animation
import org.jetbrains.compose.web.css.backgroundColor
import org.jetbrains.compose.web.css.borderRadius
import org.jetbrains.compose.web.css.bottom
import org.jetbrains.compose.web.css.duration
import org.jetbrains.compose.web.css.fillMode
import org.jetbrains.compose.web.css.iterationCount
import org.jetbrains.compose.web.css.left
import org.jetbrains.compose.web.css.margin
import org.jetbrains.compose.web.css.padding
import org.jetbrains.compose.web.css.percent
import org.jetbrains.compose.web.css.position
import org.jetbrains.compose.web.css.px
import org.jetbrains.compose.web.css.s
import org.jetbrains.compose.web.css.timingFunction
import org.jetbrains.compose.web.css.transform
import org.jetbrains.compose.web.dom.Div

@Composable
fun Notification(
    banner: NotificationViewState.Banner,
    onDismissed: () -> Unit,
) {
    var isBannerVisible by remember(banner) { mutableStateOf(true) }

    LaunchedEffect(banner) {
        window.setTimeout(
            handler = {
                isBannerVisible = false
                onDismissed()
            },
            timeout = 5000
        )
        isBannerVisible = true
    }

    val style = AppTheme.style
    val size = AppTheme.size
    val colors = AppTheme.colors

    Div({
        classes(
            if (isBannerVisible) NotificationStylesheet.display
            else NotificationStylesheet.hide
        )
        style {
            position(Position.Fixed)
            bottom(0.px)
            left(50.percent)
            property("transform", "translate(-50%, 200%)")

            backgroundColor(when (banner.type) {
                BannerState.Default -> colors.bgBannerDefault
                BannerState.Error -> colors.bgBannerError
            })
            borderRadius(size.S)
            padding(size.S, size.M)
            margin(size.XL)
        }
    }) {
        TextView(
            text = banner.message,
            textStyle = style.text.banner
        )
    }
}

object NotificationStylesheet : StyleSheet(Containers) {
    @OptIn(ExperimentalComposeWebApi::class)
    private val displayKeyframes by keyframes {
        each(0.percent) { transform { translate((-50).percent, (200).percent) } }
        each(100.percent) { transform { translate((-50).percent, 0.percent) } }
    }
    val display by style {
        animation(displayKeyframes) {
            duration(0.7.s)
            timingFunction(AnimationTimingFunction.EaseInOut)
            iterationCount(1)
            fillMode(AnimationFillMode.Forwards)
        }
    }

    @OptIn(ExperimentalComposeWebApi::class)
    private val hideKeyframes by keyframes {
        each(0.percent) { transform { translate((-50).percent, 0.percent) } }
        each(100.percent) { transform { translate((-50).percent, (200).percent) } }
    }
    val hide by style {
        animation(hideKeyframes) {
            duration(0.7.s)
            timingFunction(AnimationTimingFunction.EaseInOut)
            iterationCount(1)
            fillMode(AnimationFillMode.Forwards)
        }
    }
}