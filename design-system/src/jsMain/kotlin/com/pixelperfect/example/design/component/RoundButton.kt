package com.pixelperfect.example.design.component

import androidx.compose.runtime.Composable
import com.pixelperfect.example.design.foundation.AppTheme
import org.jetbrains.compose.web.attributes.ATarget
import org.jetbrains.compose.web.attributes.target
import org.jetbrains.compose.web.css.CSSColorValue
import org.jetbrains.compose.web.css.backgroundColor
import org.jetbrains.compose.web.css.border
import org.jetbrains.compose.web.css.color
import org.jetbrains.compose.web.css.padding
import org.jetbrains.compose.web.dom.A

@Composable
fun RoundButton(
    text: String,
    loading: Boolean = false,
    buttonSize: ButtonSize = ButtonSize.XL,
    bgColor: CSSColorValue = AppTheme.colors.button,
    onClick: () -> Unit
) {
    val size = AppTheme.size
    val style = AppTheme.style
    A(attrs = {
        classes(style.text.button)
        style {
            backgroundColor(bgColor)
            border { color(bgColor) }
            when (buttonSize) {
                ButtonSize.Small -> {
                    padding(size.S, size.L)
                }
                ButtonSize.XL -> {
                    padding(size.M, size.XXL)
//                    width(size.buttonSigninFormWidth)
                }
            }
        }
        target(ATarget.Blank)
        onClick { onClick() }
    }) {
        if (loading) {
            LoadingSpinner(
                size = AppTheme.size.M,
                strokeSize = AppTheme.size.XXS,
                loaderColor = AppTheme.colors.buttonLoader
            )
        }

        TextView(
            text = text.uppercase(),
            textStyle = AppTheme.style.text.buttonText,
            styles = arrayOf(
                when (buttonSize) {
                    ButtonSize.Small -> style.text.buttonSizeSmall
                    ButtonSize.XL -> style.text.buttonSizeXl
                }
            )
        )
    }
}

enum class ButtonSize {
    Small,
    XL
}
