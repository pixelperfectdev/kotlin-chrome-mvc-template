package com.pixelperfect.example.design.component

import androidx.compose.runtime.Composable
import com.pixelperfect.example.design.foundation.AppTheme
import org.jetbrains.compose.web.css.AlignItems
import org.jetbrains.compose.web.css.CSSNumeric
import org.jetbrains.compose.web.css.DisplayStyle
import org.jetbrains.compose.web.css.FlexDirection
import org.jetbrains.compose.web.css.JustifyContent
import org.jetbrains.compose.web.css.alignItems
import org.jetbrains.compose.web.css.display
import org.jetbrains.compose.web.css.flexDirection
import org.jetbrains.compose.web.css.gap
import org.jetbrains.compose.web.css.justifyContent
import org.jetbrains.compose.web.css.margin
import org.jetbrains.compose.web.css.padding
import org.jetbrains.compose.web.css.px
import org.jetbrains.compose.web.dom.ContentBuilder
import org.jetbrains.compose.web.dom.Div
import org.jetbrains.compose.web.dom.H1
import org.jetbrains.compose.web.dom.H2
import org.jetbrains.compose.web.dom.H3
import org.jetbrains.compose.web.dom.Span
import org.jetbrains.compose.web.dom.Text
import org.w3c.dom.HTMLHeadingElement

@Composable
fun TextView(
    text: String,
    classId: String? = null,
    textStyle: String = AppTheme.style.text.text1,
    styles: Array<String> = emptyArray(),
    paddingValues: Array<CSSNumeric> = arrayOf(AppTheme.size.None, AppTheme.size.None, AppTheme.size.None, AppTheme.size.None)
) {
    Span({
        classes(textStyle, *(styles))
        style {
            padding(*paddingValues)
        }
        if (classId != null) id(classId)
    }) {
        Text(text)
    }
}

@Composable
fun TitleView(
    text: String,
    title: Title,
    styles: Array<String> = emptyArray(),
    paddingValues: Array<CSSNumeric> = arrayOf(0.px, 0.px, 0.px, 0.px)
) {
    val content: ContentBuilder<HTMLHeadingElement> = { Text(text) }
    val style = AppTheme.style
    when (title) {
        Title.H1 -> H1({
            classes(style.text.title, *styles)
            style {
                padding(*paddingValues)
                margin(0.px)
            }
        }, content)
        Title.H2 -> H2({
            classes(style.text.heading, *styles)
            style {
                padding(*paddingValues)
                margin(0.px)
            }
        }, content)
        Title.H3 -> H3({
            classes(style.text.heading2, *styles)
            style {
                padding(*paddingValues)
                margin(0.px)
            }
        }, content)
    }
}

@Composable
fun EmptyView(text: String, onRefreshClick: (() -> Unit)? = null) {
    val size = AppTheme.size
    Div({
        style {
            display(DisplayStyle.Flex)
            flexDirection(FlexDirection.Column)
            alignItems(AlignItems.Center)
            margin(size.XXXXL)
            justifyContent(JustifyContent.Center)
            gap(size.M)
        }
    }) {
        TextView(text = text)

        if (onRefreshClick != null) TextButton(
            text = AppTheme.translation.common.retry,
            onButtonClick = onRefreshClick
        )
    }
}

@Composable
fun ErrorView(text: String, onRefreshClick: (() -> Unit)? = null) {
    val size = AppTheme.size
    Div({
        style {
            display(DisplayStyle.Flex)
            flexDirection(FlexDirection.Column)
            alignItems(AlignItems.Center)
            margin(size.XXXXL)
            justifyContent(JustifyContent.Center)
            gap(size.M)
        }
    }) {
        TextView(text)

        if (onRefreshClick != null) TextButton(
            text = AppTheme.translation.common.retry,
            onButtonClick = onRefreshClick
        )
    }
}

@Composable
fun TextButton(
    text: String,
    onButtonClick: () -> Unit
) {
    val style = AppTheme.style
    val size = AppTheme.size
    Div({
//        classes(style.common.clickable, style.container.clickableIcon)
        onClick { onButtonClick() }
        style {
            padding(size.S, size.M)
        }
    }) {
        TextView(
            text = text,
            textStyle = style.text.text2,
            styles = arrayOf(style.text.textTertiary)
        )
    }
}

enum class Title {
    H1,
    H2,
    H3,
}