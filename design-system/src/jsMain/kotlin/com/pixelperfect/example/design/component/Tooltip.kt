package com.pixelperfect.example.design.component

import androidx.compose.runtime.Composable
import com.pixelperfect.example.design.foundation.AppTheme
import org.jetbrains.compose.web.css.DisplayStyle
import org.jetbrains.compose.web.css.display
import org.jetbrains.compose.web.dom.Div

@Composable
fun Tooltip(
    title: String,
    content: @Composable () -> Unit
) {
    val style = AppTheme.style
    Div({
        classes(style.container.tooltipContainer)
    }) {
        Div({
            style { display(DisplayStyle.Flex) }
        }) {
            content()
        }
        if (title.isNotBlank()) {
            Div({
                classes(style.container.tooltipPopup, style.container.tooltipPopupBackground)
            }) {
                TextView(
                    text = title,
                    textStyle = AppTheme.style.text.tooltipPopupText,
                    paddingValues = arrayOf(AppTheme.size.SS, AppTheme.size.S)
                )
            }
        }
    }
}
