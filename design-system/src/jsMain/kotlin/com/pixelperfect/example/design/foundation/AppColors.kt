package com.pixelperfect.example.design.foundation

import org.jetbrains.compose.web.css.Color

class AppColor(val isDarkMode: Boolean = false) {
    // Common
    val primary = Blue500
    val primaryVariant = if (isDarkMode) Blue500 else Blue400
    val background = if (isDarkMode) DarkBackground else LightBackground
    val border = if (isDarkMode) White80 else Black80
    val hover = if (isDarkMode) White10 else Black10
    val loader = if (isDarkMode) White else Black
    val transparent = Transparent
    val success = Green
    val warning = Orange
    val yellow = Yellow

    // Backgrounds
    val bgHeader = Black05
    val bgBannerDefault = DarkBackground
    val bgBannerError = Red
    val bgDelete = Red
    val bgLabel = if (isDarkMode) White20 else Black20

    // Search
    val searchText = if (isDarkMode) White80 else Black80

    // Text
    val disabled = Gray500
    val text = if (isDarkMode) White else Black
    val textSecondary = if (isDarkMode) Black else White
    val textTertiary = if (isDarkMode) White60 else Black60
    val link = if (isDarkMode) White70 else Black70
    val label = White

    // Table
    val tableHeader = transparent
    val tableRowOdd = if (isDarkMode) White05 else Black05
    val tableRowEven = transparent

    // Divider
    val divider = if (isDarkMode) White20 else Black20

    // Tooltip
    val tooltipBg = if (isDarkMode) White90 else Black90
    val tooltipText = if (isDarkMode) Black else White

    // Button
    val buttonText = if (isDarkMode) Black else White
    val button = if (isDarkMode) White else Black
    val buttonLoader = if (isDarkMode) Black80 else White80
    val buttonHover = if (isDarkMode) White90 else Black90

    // Input
    val inputText = if (isDarkMode) White else Black
    val inputBg = if (isDarkMode) White10 else Black10

    // Checkbox
    val checked = if (isDarkMode) LightBlue else DarkBlue
    val unchecked = if (isDarkMode) LightBlue else DarkBlue
    val checkmark = if (isDarkMode) Black else White

    // Tag
    val tag = if (isDarkMode) White20 else Black20

    companion object {
        val Black = Color("#000000FF")
        val Black90 = Color("#000000E6")
        val Black80 = Color("#000000CC")
        val Black70 = Color("#000000B3")
        val Black60 = Color("#00000099")
        val Black50 = Color("#00000080")
        val Black40 = Color("#00000066")
        val Black30 = Color("#0000004D")
        val Black20 = Color("#00000033")
        val Black10 = Color("#0000001A")
        val Black05 = Color("#0000000D")

        val White = Color("#FFFFFFFF")
        val White90 = Color("#FFFFFFE6")
        val White80 = Color("#FFFFFFCC")
        val White70 = Color("#FFFFFFB3")
        val White60 = Color("#FFFFFF99")
        val White50 = Color("#FFFFFF80")
        val White40 = Color("#FFFFFF66")
        val White30 = Color("#FFFFFF4D")
        val White20 = Color("#FFFFFF33")
        val White10 = Color("#FFFFFF1A")
        val White05 = Color("#FFFFFF0D")

        val Gray100 = Color("#EFEFEF")
        val Gray200 = Color("#E8E8E8")
        val Gray300 = Color("#DFDFDF")
        val Gray400 = Color("#CFCFCF")
        val Gray500 = Color("#BFBFBF")
        val Gray800 = Color("#7F7F7F")

        val Blue200 = Color("#E3E9FFFF")
        val Blue300 = Color("#8AA0F1FF")
        val Blue400 = Color("#4D71F3FF")
        val Blue500 = Color("#2250F4FF")
        val Blue600 = Color("#0028B5FF")

        val LightBackground = Color("#F1F1F9FF")
        val DarkBackground = Color("#0202F9FF")
        val LightBackground2 = Color("#FEFEF9FF")
        val DarkBackground2 = Color("#0E0EF9FF")

        val LightBlue = Color("#3D65F5FF")
        val LightBlue90 = Color("#3D65F5E6")
        val DarkBlue = Color("#020A27FF")

        val Red = Color("#DF2935FF")
        val Green = Color("#29E0B9FF")
        val Orange = Color("#F0A202FF")
        val Yellow = Color("#E8D33FFF")

        val Transparent = Color("#00000000")

        val completed = Green
        val error = Red
        val building = Blue400
        val canceled = Gray500
        val pending = Yellow
        val unknown = Orange
    }
}

