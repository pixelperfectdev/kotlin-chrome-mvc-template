package com.pixelperfect.example.design.foundation

import androidx.compose.runtime.Immutable
import org.jetbrains.compose.web.css.CSSSizeValue
import org.jetbrains.compose.web.css.CSSUnit
import org.jetbrains.compose.web.css.cssRem
import org.jetbrains.compose.web.css.px

private val ref = 16f

@Immutable
data class AppSize constructor(
    // Common size
    val None: CSSSizeValue<CSSUnit.rem> = 0.cssRem,
    val XXXS: CSSSizeValue<CSSUnit.rem> = (1/ ref).cssRem,
    val XXS: CSSSizeValue<CSSUnit.rem> = (2/ ref).cssRem,
    val XS: CSSSizeValue<CSSUnit.rem> = (4/ ref).cssRem,
    val SS: CSSSizeValue<CSSUnit.rem> = (6/ ref).cssRem,
    val S: CSSSizeValue<CSSUnit.rem> = (8/ ref).cssRem,
    val M: CSSSizeValue<CSSUnit.rem> = (12/ ref).cssRem,
    val L: CSSSizeValue<CSSUnit.rem> = (16/ ref).cssRem,
    val XL: CSSSizeValue<CSSUnit.rem> = (24/ ref).cssRem,
    val XXL: CSSSizeValue<CSSUnit.rem> = (32/ ref).cssRem,
    val XXXL: CSSSizeValue<CSSUnit.rem> = (48/ ref).cssRem,
    val XXXXL: CSSSizeValue<CSSUnit.rem> = (64/ ref).cssRem,

    // Screen
    val screenSmall: CSSSizeValue<CSSUnit.px> = 340.px,
    val screenMedium: CSSSizeValue<CSSUnit.px> = 640.px,
    val screenLarge: CSSSizeValue<CSSUnit.px> = 1000.px,
    val screenXLarge: CSSSizeValue<CSSUnit.px> = 1276.px,

    // Dialog
    val dialogWidth: CSSSizeValue<CSSUnit.px> = 275.px,
    val dialogHeight: CSSSizeValue<CSSUnit.px> = 140.px,

    // Font titles
    val fontTitle: CSSSizeValue<CSSUnit.rem> = 2.2.cssRem,
    val fontTitleResponsive: CSSSizeValue<CSSUnit.rem> = 1.8.cssRem,
    val fontHeading: CSSSizeValue<CSSUnit.rem> = 1.4.cssRem,
    val fontHeadingResponsive: CSSSizeValue<CSSUnit.rem> = 1.2.cssRem,
    val fontHeading2: CSSSizeValue<CSSUnit.rem> = 1.cssRem,
    val fontHeading2Responsive: CSSSizeValue<CSSUnit.rem> = 1.cssRem,

    // Font body
    val fontSummary : CSSSizeValue<CSSUnit.rem> = 1.05.cssRem,
    val fontBody : CSSSizeValue<CSSUnit.rem> = 1.cssRem,
    val fontDescription : CSSSizeValue<CSSUnit.rem> = 0.9.cssRem,
    val fontFootnote : CSSSizeValue<CSSUnit.rem> = 0.8.cssRem,

    // Common style
    val headerLogo: CSSSizeValue<CSSUnit.rem> = (40/ ref).cssRem,
    val borderStroke: CSSSizeValue<CSSUnit.rem> = (0.9/ ref).cssRem,
    val divider: CSSSizeValue<CSSUnit.rem> = (1/ ref).cssRem,

    )