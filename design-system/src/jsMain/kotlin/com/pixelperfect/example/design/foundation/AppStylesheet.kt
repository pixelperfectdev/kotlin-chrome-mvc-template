package com.pixelperfect.example.design.foundation

import org.jetbrains.compose.web.ExperimentalComposeWebApi
import org.jetbrains.compose.web.css.LineStyle
import org.jetbrains.compose.web.css.StyleSheet
import org.jetbrains.compose.web.css.backgroundColor
import org.jetbrains.compose.web.css.border
import org.jetbrains.compose.web.css.borderRadius
import org.jetbrains.compose.web.css.color
import org.jetbrains.compose.web.css.cursor
import org.jetbrains.compose.web.css.deg
import org.jetbrains.compose.web.css.filter
import org.jetbrains.compose.web.css.percent
import org.jetbrains.compose.web.css.style
import org.jetbrains.compose.web.css.value
import org.jetbrains.compose.web.css.width

class AppStylesheet(
    colors: AppColor,
    size: AppSize
): StyleSheet() {

    val text: TextStyle = TextStyle(colors, size)
    val container: ContainerStyle = ContainerStyle(colors, size)

    val roundBorder by style {
        border {
            borderRadius(size.XL)
            style(LineStyle.Solid)
            width(size.borderStroke)
            color(colors.border)
        }
    }

    val shadow by style {
        property(
            "box-shadow",
            "0px 16px 24px rgba(0, 0, 0, 0.08), 0px 0px 6px rgba(0, 0, 0, 0.06), 0px 0px 1px rgba(0, 0, 0, 0.06)"
        )
    }

    // Blur
    val blurLight by style {
        property("backdrop-filter", "blur(4px)")
    }
    val blurMedium by style {
        property("backdrop-filter", "blur(20px)")
    }
    val blurStrong by style {
        property("backdrop-filter", "blur(200px)")
    }

    // Clicks
    val hoverable by style {
        hover(self) style {
            backgroundColor(colors.hover)
        }
    }
    val clickable by style {
        hover(self) style {
            cursor("pointer")
        }
    }

    @OptIn(ExperimentalComposeWebApi::class)
    val tintDarkTheme by style {
        filter {
            brightness(0)
            saturate(100.percent)
            invert(98.percent)
            sepia(2.percent)
            saturate(9.percent)
            hueRotate(29.deg)
            brightness(102.percent)
            contrast(104.percent)
        }
    }

    @OptIn(ExperimentalComposeWebApi::class)
    val tintLightTheme by style {
        filter {
            brightness(0)
            saturate(100.percent)
            invert(0.percent)
            sepia(1.percent)
            saturate(7498.percent)
            hueRotate(336.deg)
            brightness(91.percent)
            contrast(94.percent)
        }
    }

}