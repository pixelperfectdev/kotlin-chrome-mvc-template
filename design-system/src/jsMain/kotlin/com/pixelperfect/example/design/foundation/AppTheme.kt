package com.pixelperfect.example.design.foundation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.staticCompositionLocalOf
import com.pixelperfect.example.translation.Translation
import org.jetbrains.compose.web.css.LineStyle
import org.jetbrains.compose.web.css.Style
import org.jetbrains.compose.web.css.backgroundColor
import org.jetbrains.compose.web.css.border
import org.jetbrains.compose.web.css.borderRadius
import org.jetbrains.compose.web.css.style
import org.jetbrains.compose.web.css.width
import org.jetbrains.compose.web.dom.Div

object AppTheme {
    val colors: AppColor
        @Composable
        @ReadOnlyComposable
        get() = LocalColors.current

    val size: AppSize
        @Composable
        @ReadOnlyComposable
        get() = LocalSize.current

    val translation: Translation
        @Composable
        @ReadOnlyComposable
        get() = LocalTranslation.current

    val style: AppStylesheet
        @Composable
        @ReadOnlyComposable
        get() = LocalStylesheet.current
}

@Composable
fun AppTheme(
    colors: AppColor = AppColor(),
    size: AppSize = AppSize(),
    translation: Translation = Translation(),
    appStyle: AppStylesheet = AppStylesheet(colors, size),
    content: @Composable () -> Unit
) {
    CompositionLocalProvider(
        LocalColors provides colors,
        LocalSize provides size,
        LocalTranslation provides translation,
        LocalStylesheet provides appStyle,
    ) {
        // Setup UI
        Style(RootDialogStyle)
        Style(appStyle)
        Style(appStyle.container)
        Style(Containers)
        Style(appStyle.text)

        Div({
            classes(appStyle.shadow)
            style {
                backgroundColor(colors.background)
                border {
                    borderRadius(size.S)
                    style(LineStyle.None)
                    width(size.borderStroke)
                }
            }
        }) {
            content()
        }
    }
}

internal val LocalColors = staticCompositionLocalOf { AppColor() }
internal val LocalSize = staticCompositionLocalOf { AppSize() }
internal val LocalTranslation = staticCompositionLocalOf { Translation() }
internal val LocalStylesheet = staticCompositionLocalOf { AppStylesheet(AppColor(), AppSize()) }
