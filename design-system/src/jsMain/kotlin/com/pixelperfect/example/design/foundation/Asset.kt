package com.pixelperfect.example.design.foundation

enum class Asset {
    IcClose,
    IcDown,
    IcSettings,
    IcReset,
}