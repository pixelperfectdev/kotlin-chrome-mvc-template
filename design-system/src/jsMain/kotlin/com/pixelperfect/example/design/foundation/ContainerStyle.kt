package com.pixelperfect.example.design.foundation

import org.jetbrains.compose.web.css.AlignItems
import org.jetbrains.compose.web.css.DisplayStyle
import org.jetbrains.compose.web.css.FlexDirection
import org.jetbrains.compose.web.css.FlexWrap
import org.jetbrains.compose.web.css.JustifyContent
import org.jetbrains.compose.web.css.LineStyle
import org.jetbrains.compose.web.css.Position
import org.jetbrains.compose.web.css.StyleSheet
import org.jetbrains.compose.web.css.alignItems
import org.jetbrains.compose.web.css.backgroundColor
import org.jetbrains.compose.web.css.border
import org.jetbrains.compose.web.css.borderRadius
import org.jetbrains.compose.web.css.boxSizing
import org.jetbrains.compose.web.css.display
import org.jetbrains.compose.web.css.flexDirection
import org.jetbrains.compose.web.css.flexWrap
import org.jetbrains.compose.web.css.height
import org.jetbrains.compose.web.css.justifyContent
import org.jetbrains.compose.web.css.maxWidth
import org.jetbrains.compose.web.css.media
import org.jetbrains.compose.web.css.mediaMaxWidth
import org.jetbrains.compose.web.css.padding
import org.jetbrains.compose.web.css.paddingLeft
import org.jetbrains.compose.web.css.paddingRight
import org.jetbrains.compose.web.css.percent
import org.jetbrains.compose.web.css.position
import org.jetbrains.compose.web.css.style
import org.jetbrains.compose.web.css.vh
import org.jetbrains.compose.web.css.width

object Containers: StyleSheet() {
    val column by style {
        display(DisplayStyle.Flex)
        flexDirection(FlexDirection.Column)
        alignItems(AlignItems.Center)
        justifyContent(JustifyContent.Start)
    }

    val row by style {
        display(DisplayStyle.Flex)
        flexDirection(FlexDirection.Row)
        alignItems(AlignItems.Center)
        justifyContent(JustifyContent.Start)
        flexWrap(FlexWrap.Wrap)
    }

    val maxWidth by style {
        width(100.percent)
        boxSizing("border-box")
    }

    val maxHeight by style {
        height(100.vh)
    }

    // Blurs

    val blurLight by style {
        property("backdrop-filter", "blur(4px)")
    }
}

class ContainerStyle(
    colors: AppColor,
    size: AppSize,
): StyleSheet() {

    val tooltipContainer = "contentViewTooltipContainer"
    val tooltipPopup = "contentViewTooltipPopup"
    val tooltipPopupBackground by style {
        backgroundColor(colors.tooltipBg)
        border {
            borderRadius(size.XS)
            style(LineStyle.Solid)
            width(size.borderStroke)
        }
    }

    val dialog by style {
        position(Position.Relative)
        padding(size.S, size.M)
        width(size.dialogWidth)
        height(size.dialogHeight)
        border {
            borderRadius(size.S)
            style(LineStyle.None)
            width(size.borderStroke)
        }
        backgroundColor(colors.background)
    }

    val containerInSection by style {
        property("margin-left", "auto")
        property("margin-right", "auto")
        boxSizing("border-box")
        paddingLeft(size.XXXXL)
        paddingRight(size.XXXXL)
        maxWidth(size.screenLarge)

        media(mediaMaxWidth(size.screenMedium)) {
            self style {
                maxWidth(100.percent)
                paddingLeft(size.XL)
                paddingRight(size.XL)
            }
        }

        media(mediaMaxWidth(size.screenLarge)) {
            self style {
                maxWidth(100.percent)
                paddingLeft(size.XXL)
                paddingRight(size.XXL)
            }
        }

        media(mediaMaxWidth(size.screenXLarge)) {
            self style {
                paddingLeft(size.XXXL)
                paddingRight(size.XXXL)
            }
        }
    }

}
