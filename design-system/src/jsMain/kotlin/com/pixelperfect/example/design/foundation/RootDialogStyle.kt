package com.pixelperfect.example.design.foundation

import org.jetbrains.compose.web.css.StyleSheet

object RootDialogStyle: StyleSheet() {

    init {
        ".exampleHighlight" style  {
            property("background", "rgba(0, 250, 10, 0.2)")
        }

        "#exampleContainer" style {
            property("position", "absolute")
            property("z-index", "2147483645")
        }

        "#exampleContainer #exampleDialog" style {
            property("position", "fixed")
            property("bottom", "30px")
            property("right", "30px")
            property("border-radius", "0.5rem")
        }

        ".exampleTooltipContainer" style {
        }

        ".exampleTooltipContainer .exampleTooltipPopup" style {
            property("visibility", "hidden")
            property("position", "absolute")
            property("z-index", "1")
        }

        ".exampleTooltipContainer:hover .exampleTooltipPopup" style {
            property("visibility", "visible")
        }
    }
}