package com.pixelperfect.example.design.foundation

import org.jetbrains.compose.web.css.AlignItems
import org.jetbrains.compose.web.css.DisplayStyle
import org.jetbrains.compose.web.css.FlexDirection
import org.jetbrains.compose.web.css.JustifyContent
import org.jetbrains.compose.web.css.LineStyle
import org.jetbrains.compose.web.css.StyleSheet
import org.jetbrains.compose.web.css.alignItems
import org.jetbrains.compose.web.css.backgroundColor
import org.jetbrains.compose.web.css.border
import org.jetbrains.compose.web.css.borderRadius
import org.jetbrains.compose.web.css.color
import org.jetbrains.compose.web.css.cursor
import org.jetbrains.compose.web.css.display
import org.jetbrains.compose.web.css.flexDirection
import org.jetbrains.compose.web.css.fontFamily
import org.jetbrains.compose.web.css.fontSize
import org.jetbrains.compose.web.css.fontWeight
import org.jetbrains.compose.web.css.gap
import org.jetbrains.compose.web.css.justifyContent
import org.jetbrains.compose.web.css.letterSpacing
import org.jetbrains.compose.web.css.media
import org.jetbrains.compose.web.css.mediaMaxWidth
import org.jetbrains.compose.web.css.px
import org.jetbrains.compose.web.css.style
import org.jetbrains.compose.web.css.textAlign
import org.jetbrains.compose.web.css.textDecoration
import org.jetbrains.compose.web.css.unaryMinus
import org.jetbrains.compose.web.css.width

class TextStyle(
    colors: AppColor,
    size: AppSize,
): StyleSheet() {

    private val headerFonts = "sans-serif"
    private val defaultFonts = "sans-serif"

    val title by style {
        color(colors.text)
        fontSize(size.fontTitle)
        letterSpacing(-size.XXS)
        fontWeight(800)

        media(mediaMaxWidth(640.px)) {
            self style {
                fontSize(size.fontTitleResponsive)
            }
        }
    }

    val heading by style {
        color(colors.text)
        fontSize(size.fontHeading)
        letterSpacing("normal")
        fontWeight(550)

        media(mediaMaxWidth(640.px)) {
            self style {
                fontSize(size.fontHeadingResponsive)
            }
        }

        property("font-family", headerFonts)
    }

    val heading2 by style {
        color(colors.text)
        fontSize(size.fontHeading2)
        letterSpacing("normal")
        fontWeight(400)

        media(mediaMaxWidth(640.px)) {
            self style {
                fontSize(size.fontHeading2Responsive)
            }
        }

        property("font-family", headerFonts)
    }

    val text1 by style {
        fontFamily(defaultFonts)
        color(colors.text)
        fontSize(size.fontBody)
        letterSpacing("normal")
        fontWeight(400)
        textAlign("start")
    }

    val text2 by style {
        fontFamily(defaultFonts)
        color(colors.text)
        fontSize(size.fontDescription)
        letterSpacing("normal")
        fontWeight(400)
        textAlign("start")
    }

    val text3 by style {
        fontFamily(defaultFonts)
        color(colors.text)
        fontSize(size.fontFootnote)
        letterSpacing("normal")
        fontWeight(400)
        textAlign("start")
    }

    val textDefault by style {
        fontFamily(defaultFonts)
        color(colors.text)
    }

    val textTertiary by style {
        fontFamily(defaultFonts)
        color(colors.textTertiary)
    }

    val tooltipPopupText by style {
        fontFamily(defaultFonts)
        color(colors.tooltipText)
        fontSize(size.fontFootnote)
        letterSpacing("normal")
        fontWeight(400)
        display(DisplayStyle.Block)
        textAlign("start")
    }

    val button by style {
        fontFamily(defaultFonts)
        display(DisplayStyle.Flex)
        flexDirection(FlexDirection.Row)
        alignItems(AlignItems.Center)
        justifyContent(JustifyContent.Center)
        backgroundColor(colors.button)
        gap(size.M)
        property("width", "fit-content")

        border {
            borderRadius(size.XL)
            style(LineStyle.Solid)
            width(size.borderStroke)
            color(colors.border)
        }

        hover(self) style {
            cursor("pointer")
            backgroundColor(colors.buttonHover)
        }
    }

    val buttonText by style {
        fontFamily(defaultFonts)
        color(colors.textSecondary)
        fontWeight(550)
        letterSpacing("normal")
        textDecoration("none")
        textAlign("center")
    }

    val buttonSizeXl by style {
        fontFamily(defaultFonts)
        fontSize(size.fontHeading2)
    }

    val buttonSizeSmall by style {
        fontFamily(defaultFonts)
        fontSize(size.fontFootnote)
    }

    val banner by style {
        fontFamily(defaultFonts)
        color(colors.textSecondary)
        fontSize(size.fontDescription)
        letterSpacing("normal")
        fontWeight(400)
        textAlign("start")
    }
}