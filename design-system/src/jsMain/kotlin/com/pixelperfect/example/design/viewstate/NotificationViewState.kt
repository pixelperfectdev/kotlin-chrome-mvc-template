package com.pixelperfect.example.design.viewstate

sealed class NotificationViewState {

    object None: NotificationViewState()

    sealed class Dialog: NotificationViewState() {
        data class FilePicker(val onFilePicked: (String?) -> Unit): Dialog()
        object FolderPicker: Dialog()
    }

    sealed class Banner(val message: String, val type: BannerState) : NotificationViewState() {
        class Default(message: String) : Banner(message, BannerState.Default)
        class Error(message: String) : Banner(message, BannerState.Error)
    }
}

sealed class BannerState {
    object Default: BannerState()
    object Error: BannerState()
}