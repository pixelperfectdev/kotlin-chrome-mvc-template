package com.pixelperfect.example.domain.presenter

import com.pixelperfect.example.common.Presenter
import kotlinx.coroutines.flow.StateFlow

interface ContentPresenter: Presenter {

    val viewState: StateFlow<ContentViewState>
    val errorState: StateFlow<ErrorContentViewState>

    fun displayError(message: String?)
    fun notificationDismissed()

}