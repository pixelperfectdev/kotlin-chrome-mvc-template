package com.pixelperfect.example.domain.presenter

data class ContentViewState(
    val message: String? = null
)


data class ErrorContentViewState(
    val message: String? = null
)