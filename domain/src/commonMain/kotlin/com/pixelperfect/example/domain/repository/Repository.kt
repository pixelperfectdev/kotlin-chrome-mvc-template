package com.pixelperfect.example.domain.repository

interface Repository {
    suspend fun clear()
}