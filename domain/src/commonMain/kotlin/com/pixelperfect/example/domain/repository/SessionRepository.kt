package com.pixelperfect.example.domain.repository

interface SessionRepository: Repository {

    suspend fun setDarkTheme(enabled: Boolean)
    suspend fun isDarkThemeEnabled(): Boolean

}