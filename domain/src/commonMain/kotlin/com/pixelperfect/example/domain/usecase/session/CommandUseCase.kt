package com.pixelperfect.example.domain.usecase.session

interface CommandUseCase {

    suspend fun close()
    suspend fun openSettings()

}