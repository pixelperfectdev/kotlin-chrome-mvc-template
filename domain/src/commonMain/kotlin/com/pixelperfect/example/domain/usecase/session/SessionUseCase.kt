package com.pixelperfect.example.domain.usecase.session

interface SessionUseCase {

    suspend fun stop()
}