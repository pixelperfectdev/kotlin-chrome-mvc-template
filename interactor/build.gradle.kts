plugins {
    kotlin("js")
    kotlin("plugin.serialization")
}

dependencies {
    implementation(project(":common"))
    implementation(project(":domain"))
    implementation(project(":chrome"))
    implementation(project(":translation"))
}

kotlin {
    js(IR) {
        binaries.executable()
        browser()
    }
}
