package com.pixelperfect.example.interactor.di

import com.pixelperfect.example.domain.usecase.session.SessionUseCase
import com.pixelperfect.example.interactor.session.SessionInteractor
import org.koin.dsl.module

val interactorModule = module {

    factory<SessionUseCase> {
        SessionInteractor()
    }

}