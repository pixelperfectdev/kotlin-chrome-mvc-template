package com.pixelperfect.example.interactor.session

import chrome.sendMessageToBackground
import com.pixelperfect.example.data.Action
import com.pixelperfect.example.data.Command
import com.pixelperfect.example.domain.presenter.ContentPresenter
import com.pixelperfect.example.domain.usecase.session.CommandUseCase

class CommandInteractor(
    private val presenter: ContentPresenter
): CommandUseCase {

    override suspend fun close() {
        sendMessageToBackground(message = Command(action = Action.CLOSE_CONTENT_VIEW))
    }

    override suspend fun openSettings() {
        sendMessageToBackground(message = Command(action = Action.OPEN_OPTIONS))
    }
}