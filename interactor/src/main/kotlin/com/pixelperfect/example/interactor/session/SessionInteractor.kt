package com.pixelperfect.example.interactor.session

import com.pixelperfect.example.common.Constants
import com.pixelperfect.example.domain.usecase.session.SessionUseCase
import kotlinx.browser.document

class SessionInteractor(
): SessionUseCase {

    override suspend fun stop() {
        document.getElementById(Constants.CONTENT_VIEW_ID)?.remove()
    }

}