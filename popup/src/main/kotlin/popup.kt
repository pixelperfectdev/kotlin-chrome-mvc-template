import androidx.compose.runtime.NoLiveLiterals
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import com.pixelperfect.example.data.Action
import com.pixelperfect.example.data.Command
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.jetbrains.compose.web.dom.Button
import org.jetbrains.compose.web.dom.Text
import org.jetbrains.compose.web.renderComposable

fun main() {
    renderComposable("root") {
        var result by remember { mutableStateOf("") }
        Button({
            onClick {
                sendMessage {
                    result = "Message result: ${it.action}"
                }
            }
        }) {
            Text("Send message")
        }

        Text(result)
    }
}

@NoLiveLiterals
private fun sendMessage(onResponse: (Command) -> Unit) {
    val message = Command(action = Action.OPEN_OPTIONS)
    chrome.runtime.sendMessage(
        message = Json.encodeToString(message),
        responseCallback = { response ->
            val message = Json.decodeFromString<Command>(response.toString())
            onResponse(message)
        }
    )
}