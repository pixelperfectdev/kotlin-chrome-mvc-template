plugins {
    kotlin("multiplatform")
}

kotlin {
    js(IR) {
        binaries.executable()
        browser {}
    }

    sourceSets {
        named("commonMain") {
            dependencies {
                implementation(project(":common"))
                implementation(project(":domain"))
            }
        }
    }
}