package com.pixelperfect.example.repository

import com.pixelperfect.example.domain.repository.SessionRepository
import com.pixelperfect.example.repository.service.datastore.SessionDataStore

class SessionRepositoryImpl(
    private val sessionDataStore: SessionDataStore
): SessionRepository {

    override suspend fun setDarkTheme(enabled: Boolean) {
        sessionDataStore.setDarkTheme(enabled)
    }

    override suspend fun isDarkThemeEnabled(): Boolean = sessionDataStore.isDarkThemeEnabled()

    override suspend fun clear() {
        sessionDataStore.clear()
    }
}