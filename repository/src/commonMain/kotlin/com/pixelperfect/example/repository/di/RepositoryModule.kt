package com.pixelperfect.example.repository.di

import com.pixelperfect.example.domain.repository.Repository
import com.pixelperfect.example.domain.repository.SessionRepository
import com.pixelperfect.example.repository.SessionRepositoryImpl
import org.koin.dsl.bind
import org.koin.dsl.module

val repositoryModule = module {

    single<SessionRepository> {
        SessionRepositoryImpl(
            sessionDataStore = get(),
        )
    } bind Repository::class

}