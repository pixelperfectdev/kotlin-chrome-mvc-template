package com.pixelperfect.example.repository.service.datastore

interface DataStore {
    fun clear()
}