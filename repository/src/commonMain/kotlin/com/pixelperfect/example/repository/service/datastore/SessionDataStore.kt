package com.pixelperfect.example.repository.service.datastore

interface SessionDataStore: DataStore {

    suspend fun setDarkTheme(enabled: Boolean)
    suspend fun isDarkThemeEnabled(): Boolean

}