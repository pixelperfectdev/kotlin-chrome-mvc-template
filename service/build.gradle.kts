plugins {
    kotlin("multiplatform")
}

kotlin {
    js(IR) {
        binaries.executable()
        browser {}
    }

    sourceSets {
        named("commonMain") {
            dependencies {
                implementation(Deps.Kotlin.serialization)
                implementation(project(":repository"))
                implementation(project(":common"))

                // Cache
                implementation(Deps.Storage.Cache.settings)
                // Ktor client
                implementation(Deps.Network.Ktor.json)
                implementation(Deps.Network.Ktor.client_js)
                implementation(Deps.Network.Ktor.client_core)
                implementation(Deps.Network.Ktor.client_json)
                implementation(Deps.Network.Ktor.client_auth)
                implementation(Deps.Network.Ktor.client_serialization)
                implementation(Deps.Network.Ktor.client_content_negotiation)
            }
        }

        named("jsMain") {
            dependencies {
                implementation(kotlin("stdlib-js"))
                implementation(project(":chrome"))
                compileOnly(npm("extpay", "3.0.7"))
            }
        }
    }
}