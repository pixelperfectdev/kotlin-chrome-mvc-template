package com.pixelperfect.example.service

import io.ktor.client.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json

object NetworkClient {
    fun httpClient(
        customHeaders: Map<String, String> = emptyMap()
    ): HttpClient = HttpClient() {
        install(DefaultRequest)
        install(HttpTimeout) {
            socketTimeoutMillis = 30_000
            requestTimeoutMillis = 30_000
            connectTimeoutMillis = 30_000
        }
        install(ContentNegotiation) {
            json(Json {
                prettyPrint = true
                ignoreUnknownKeys = true
            })
        }
        defaultRequest {
            customHeaders.forEach { (key, value) ->
                header(key, value)
            }
            header("Content-Type", "application/json")
            contentType(ContentType.Application.Json)
            accept(ContentType.Application.Json)
        }
    }
}