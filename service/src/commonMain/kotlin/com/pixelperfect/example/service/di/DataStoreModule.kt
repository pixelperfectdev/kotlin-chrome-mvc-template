package com.pixelperfect.example.service.di

import com.pixelperfect.example.repository.service.datastore.SessionDataStore
import com.pixelperfect.example.service.datastore.SessionDataStoreImpl
import com.russhwolf.settings.Settings
import org.koin.dsl.module

val dataStoreModule = module {

    factory<SessionDataStore> {
        SessionDataStoreImpl()
    }
}