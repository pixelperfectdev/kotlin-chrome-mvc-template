package com.pixelperfect.example.service.di

import com.pixelperfect.example.service.NetworkClient
import io.ktor.client.*
import org.koin.dsl.module

val serviceModule = module {

    single<HttpClient> {
        NetworkClient.httpClient()
    }

    includes(dataStoreModule, dataSourceModule)
}