package com.pixelperfect.example.service.datastore

import com.pixelperfect.example.repository.service.datastore.SessionDataStore
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromDynamic
import kotlinx.serialization.json.encodeToDynamic

actual class SessionDataStoreImpl: SessionDataStore {

    private val KEY_SESSION = "session"

    @OptIn(ExperimentalSerializationApi::class)
    override suspend fun setDarkTheme(enabled: Boolean) {
        chrome.storage.sync.set(
            Json.encodeToDynamic(SettingsSession(SessionSettings(darkThemeEnabled = enabled)))
        )

    }

    @OptIn(ExperimentalCoroutinesApi::class, ExperimentalSerializationApi::class)
    override suspend fun isDarkThemeEnabled(): Boolean {
        return suspendCancellableCoroutine { result ->
            chrome.storage.sync.get(KEY_SESSION) { items ->
                try {
                    val json = Json.decodeFromDynamic<SettingsSession>(items)
                    result.resume(json.session.darkThemeEnabled, null)
                } catch (e: Exception) {
                    result.resume(false, null)
                }
            }
        }
    }

    override fun clear() {

    }
}

@Serializable
class SessionSettings(
    @SerialName("darkThemeEnabled") val darkThemeEnabled: Boolean = false,
)