package com.pixelperfect.example.service.datastore

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable data class SettingsSession(@SerialName("session") val session: SessionSettings)