rootProject.name = "chrome-mvc-template"

include("app")

include("background")
include("content")
include("popup")

include("chrome")
include("common")
include("data")
include("design-system")
include("domain")
include("interactor")
include("repository")
include("service")
include("translation")
