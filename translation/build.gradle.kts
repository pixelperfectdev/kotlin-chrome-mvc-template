plugins {
    kotlin("multiplatform")
}

kotlin {
    js(IR) {
        binaries.executable()
        browser {}
    }

    sourceSets {
        named("commonMain") {
            dependencies {
                implementation(Deps.Kotlin.serialization)
                implementation(project(":repository"))
            }
        }

        named("jsMain") {
            dependencies {
                implementation(kotlin("stdlib-js"))
            }
        }
    }
}