package com.pixelperfect.example.translation

open class Translation(
    open val common: CommonStrings = CommonStrings(),
)

open class CommonStrings {
    open val appName: String = ""
    open val submit: String = "Submit"
    open val clear: String = "Clear"
    open val cancel: String = "Cancel"
    open val feedbackForm: String = "https://tally.so/r/3jebP4"
    open val unexpectedError: String = "⚠️ %s"
    open val openLink: String = "Open link"
    open val logout: String = "Logout"
    open val search: String = "Search"
    open val retry: String = "Retry"
    open val edit: String = "Edit"
    open val refresh: String = "Refresh"
    open val seeAll: String = "See all"
    open val settings: String = "Settings"
    open val date: String = "Date"
    open val duration: String = "Duration"
    open val delete: String = "Delete"
    open val remove: String = "Remove"
    open val back: String = "Back"
    open val logo: String = "App logo"
    open val close: String = "Close"
    open val visibility: String = "Visibility"
    open val enableDarkMode: String = "Dark Mode"
    open val resetSelection: String = "Reset selection"
}
